/**
 * Facade Design Pattern include all method through pages and models to work with each page and API of AUT
 */
package com.taf;

import com.taf.layers.api.RestAssuredClient;
import com.taf.layers.api.ServerInterceptorCalls;
import com.taf.layers.ui.pages.AboutGooglePage;
import com.taf.layers.ui.pages.MainPage;
import lombok.Getter;

import java.util.List;

import static com.taf.configuration.DriverManager.initBrowser;
import static com.taf.configuration.DriverManager.setBrowserSize;
import static com.taf.constants.UIConst.*;

public class TAF {
    @Getter
    private RestAssuredClient restClient;
    @Getter
    private ServerInterceptorCalls interceptor;
    private MainPage mainPage;
    private AboutGooglePage aboutGooglePage;

    public TAF(boolean isUI, ServerInterceptorCalls interceptorAPICalls) {
        if (isUI) {
            initBrowser();
        } else {
            restClient = new RestAssuredClient();
            interceptor = interceptorAPICalls;
        }
    }

    public void switchToMobileView() {
        setBrowserSize(MOBILE_BROWSER_SIZE);
    }

    public void openMainPage() {
        mainPage = new MainPage(BASE_UI_URL);
        mainPage.waitUntilPageIsLoaded();
    }

    public void openAboutGooglePage() {
        aboutGooglePage = new AboutGooglePage(ABOUT_GOOGLE_PAGE_UI_URL);
        aboutGooglePage.waitUntilPageIsLoaded();
    }

    public void cleansBrowserCookies() {
        mainPage.cleansBrowserCookies();
    }

    public String getCurrentUrl() {
        return mainPage.getCurrentUrl();
    }

    public String getMainPageTitleText() {
        return mainPage.getTitle().getOwnText();
    }

    public boolean isGoogleLogoPresentOnMainPage() {
        return mainPage.isGoogleLogoPresent();
    }

    public boolean isGoogleSearchFieldPresentOnMainPage() {
        return mainPage.isGoogleSearchFieldPresent();
    }

    public int getMainPageFooterElementsCount() {
        return mainPage.getFooter().getFooterElementsCount();
    }

    public List<String> getMainPageFooterItems() {
        return mainPage.getFooter().getFooterItems();
    }

    public List<String> getFooterElementsNamesOnMainPage() {
        return mainPage.getFooter().getFooterElementsNames();
    }

    public void openAboutGooglePageFromMainPageFooter() {
        aboutGooglePage = mainPage.openAboutGooglePageFromFooter();
    }

    public String getAboutGooglePageCurrentURL() {
        return aboutGooglePage.getCurrentUrl();
    }

    public String getAboutGooglePageExpURL() {
        return AboutGooglePage.EXP_PAGE_URL;
    }

    public String getAboutGooglePageTitleText() {
        return aboutGooglePage.getTitle().getOwnText();
    }

    public String getAboutGooglePageExpTitleText() {
        return AboutGooglePage.EXP_TITLE_TEXT;
    }

    public String getAboutGooglePageHeaderText() {
        return aboutGooglePage.getHeaderFromPage().getOwnText().trim();
    }

    public String getAboutGooglePageExpectedHeaderText() {
        return AboutGooglePage.EXP_HEADER_TEXT;
    }
}
