/**
 * Utility Config class should not have public constructors
 * Using for get Properties from files and system
 */
package com.taf.configuration;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.taf.constants.TAFConst.TAF_ENV_PROPERTIES_PATH;
import static com.taf.constants.TAFConst.TAF_MAIN_PROPERTIES;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TAFConfig {

    public static String getProperty(String propertyPath, String propertyName) {
        String propertyValue = "TAF-property value was missed";
        try (InputStream inputStream = new FileInputStream(propertyPath)) {
            Properties properties = new Properties();
            properties.load(inputStream);
            propertyValue = properties.getProperty(propertyName);
        } catch (IOException ioe) {
            log.error("{} for {}!", propertyValue, propertyName);
            ioe.printStackTrace();
        }
        return propertyValue;
    }

    public static String getSystemProperty(String propertyName) {
        if (System.getProperty(propertyName) == null || System.getProperty(propertyName).isEmpty()) {
            log.warn("System property for \"{}\" is missed, so the default value from {} will be used instead",
                    propertyName, TAF_MAIN_PROPERTIES);
            System.setProperty(propertyName, getProperty(TAF_MAIN_PROPERTIES, propertyName));
        }
        return System.getProperty(propertyName);
    }

    public static String getSystemProperty(String propertyName, String defaultValue) {
        if (System.getProperty(propertyName) == null || System.getProperty(propertyName).isEmpty()) {
            return defaultValue;
        }
        return System.getProperty(propertyName);
    }

    public static String setUpEnv() {
        if (System.getProperty("env") == null || System.getProperty("env").isEmpty()) {
            log.warn("System property for \"Environment\" is missed, so the local env will be used instead");
            return String.format(TAF_ENV_PROPERTIES_PATH, "", "local");
        }
        log.info(String.format(TAF_ENV_PROPERTIES_PATH, "Environment properties are applied from: ", System.getProperty("env")));
        return String.format(TAF_ENV_PROPERTIES_PATH, "", System.getProperty("env"));
    }
}
