/**
 * Utility Config class should not have public constructors
 * Using for getting Constants throughout the TAF
 */
package com.taf.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.taf.configuration.TAFConfig.*;
import static com.taf.constants.TAFConst.TAF_ENV_PROPERTIES;
import static com.taf.constants.TAFConst.TAF_MAIN_PROPERTIES;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class APIConst {
    public static final String BASE_API_URL = getProperty(TAF_ENV_PROPERTIES, "api.url");
    public static final String MOCK_API_URL = getProperty(TAF_ENV_PROPERTIES, "api.mock.url");
    public static final String API_JSON_RESPONSES = getProperty(TAF_MAIN_PROPERTIES, "api.json.path.responses");
    public static final String RESOURCE_ENDPOINT = getProperty(TAF_ENV_PROPERTIES, "api.path.resource.endpoint");
    public static final String RESOURCES_ENDPOINT = getProperty(TAF_ENV_PROPERTIES, "api.path.resources.endpoint");
    public static final String USER_ENDPOINT = getProperty(TAF_ENV_PROPERTIES, "api.path.user.endpoint");
    public static final String RETAILER_ENDPOINT = getProperty(TAF_ENV_PROPERTIES, "api.path.retailer.endpoint");
    public static final String USER_REG_ENDPOINT = getProperty(TAF_ENV_PROPERTIES, "api.path.userRegister.endpoint");
    public static final String USER_LOGIN_ENDPOINT = getProperty(TAF_ENV_PROPERTIES, "api.path.userLogin.endpoint");
    public static final String ALL_USERS_ENDPOINT = getProperty(TAF_ENV_PROPERTIES, "api.path.allUsers.endpoint");
    public static final String CONTENT_TYPE_JSON = "application/json;charset=utf-8";
}
