package com.taf.layers.ui.components;

import com.taf.layers.ui.pages.AboutGooglePage;
import lombok.Getter;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$;

public class Footer {
    @Getter
    private final List<String> footerItems = new ArrayList<>(Arrays.asList("Про Google", "Реклама", "Для бізнесу", "Як працює Пошук", "Конфіденційність", "Умови"));
    private static final String FOOTER_ELEMENTS_LOCATOR = "//a[@class='pHiOh']";
    public int getFooterElementsCount() {
        return $$(By.xpath(FOOTER_ELEMENTS_LOCATOR)).size();
    }

    public List<String> getFooterElementsNames() {
        return $$(By.xpath(FOOTER_ELEMENTS_LOCATOR)).texts();

    }

    public AboutGooglePage openAboutGooglePage() {
        $$(By.xpath(FOOTER_ELEMENTS_LOCATOR)).get(0).click();
        return new AboutGooglePage();
    }
}
