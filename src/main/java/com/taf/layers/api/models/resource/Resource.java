package com.taf.layers.api.models.resource;

import lombok.Getter;

public class Resource {
    @Getter
    private Integer page;
    @Getter
    private Integer per_page;
    @Getter
    private Integer total;
    @Getter
    private Integer total_pages;

}
