package com.taf.layers.api.models.user;

import lombok.Getter;

public class UserData {
    @Getter
    private Integer id;
    @Getter
    private String email;
    @Getter
    private String first_name;
    @Getter
    private String last_name;
    @Getter
    private String avatar;
    @Getter
    private String job;
    @Getter
    private String updatedAt;

    public UserData(String firstName, String job) {
        this.first_name = firstName;
        this.job = job;
    }

    public String getIdFromAvatarLink() {
        return getAvatar().substring(getAvatar().lastIndexOf("/") + 1,
                getAvatar().lastIndexOf("-"));
    }
}
