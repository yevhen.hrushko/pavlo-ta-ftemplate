package com.taf.layers.api.models.resource;

import lombok.Getter;

public class ResourceData {
    @Getter
    private Integer id;
    @Getter
    private String name;
    @Getter
    private String year;
    @Getter
    private String color;
    @Getter
    private String pantone_value;

}
