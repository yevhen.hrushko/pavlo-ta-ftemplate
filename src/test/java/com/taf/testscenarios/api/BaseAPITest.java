package com.taf.testscenarios.api;

import com.taf.TAF;
import com.taf.testscenarios.CommonBaseTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static com.taf.configuration.extentreports.ExtentReportsManager.includeTestToReport;

public class BaseAPITest extends CommonBaseTest {
    protected TAF taf = new TAF(false, null);

    @BeforeMethod(alwaysRun = true)
    public void reporting(Method method) {
        includeTestToReport(method.getAnnotation(Test.class));
    }
}
