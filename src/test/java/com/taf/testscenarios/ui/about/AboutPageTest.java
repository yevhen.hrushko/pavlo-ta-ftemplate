package com.taf.testscenarios.ui.about;

import com.taf.testscenarios.ui.BaseUITest;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

public class AboutPageTest extends BaseUITest {
    @Test(suiteName = "AboutPageTests",
            testName = "Direct open About Google page test",
            description = "Requirements: Task_08",
            groups = {"Regression", "MobileView", "XBrowser"})
    public void openAboutGooglePageTest() {
        taf.openAboutGooglePage();
        Assertions.assertThat(taf.getAboutGooglePageHeaderText()).isEqualTo(taf.getAboutGooglePageExpectedHeaderText());
    }
}
