//Weekly night full regression declarative pipeline with all API, all UI, cross browser and mobile view tests
pipeline {
    agent any

    triggers {
        cron('H H(0-1) * * 1')
    }

    tools {
        maven 'Maven'
    }

    stages {
        stage('Repo pulling') {
            steps {
                git 'https://gitlab.com/pavlo.manuilenko/testautomationsolution.git'
            }
        }

        stage('Executing all API and UI tests') {
            steps {
                bat 'mvn clean test -DtestSuiteName=all_tests -Dui.browser=chrome -Dheadless=true -Denv=ci -DthreadCount=3'
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'target/extent-reports', reportFiles: 'Report.html', reportName: 'AT Report (all)', reportTitles: 'AT Report', useWrapperFileDirectly: true])
            }
        }

        stage('Executing cross browser tests (Edge)') {
            steps {
                bat 'mvn clean test -DtestSuiteName=cross_browser_tests -Dui.browser=edge -Dheadless=true -Denv=ci -DthreadCount=3'
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'target/extent-reports', reportFiles: 'Report.html', reportName: 'AT Report (edge)', reportTitles: 'AT Report', useWrapperFileDirectly: true])
            }
        }

        stage('Executing cross browser tests (Firefox)') {
            steps {
                bat 'mvn clean test -DtestSuiteName=cross_browser_tests -Dui.browser=firefox -Dheadless=true -Denv=ci -DthreadCount=3'
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'target/extent-reports', reportFiles: 'Report.html', reportName: 'AT Report (firefox)', reportTitles: 'AT Report', useWrapperFileDirectly: true])
            }
        }

        stage('Executing mobile view tests') {
            steps {
                bat 'mvn clean test -DtestSuiteName=mobile_view_tests -Dui.browser=chrome -Dheadless=true -Denv=ci -DthreadCount=3'
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'target/extent-reports', reportFiles: 'Report.html', reportName: 'AT Report (mobile view)', reportTitles: 'AT Report', useWrapperFileDirectly: true])
            }
        }

    }
}